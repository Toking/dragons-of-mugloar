package com.bigbank.dragonsofmugloar.utils;


import com.bigbank.dragonsofmugloar.model.Item;
import com.bigbank.dragonsofmugloar.model.Mission;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class ServiceHelperTest {
    private ServiceHelper serviceHelper = new ServiceHelper();

    private ArrayList<Mission> createMissions() {
        Mission m = new Mission();
        Mission m2 = new Mission();
        Mission m3 = new Mission();

        m.setProbability("Piece of cake");
        m2.setProbability("Walk in the park");
        m3.setProbability("Quite likely");

        ArrayList<Mission> missions = new ArrayList<>();
        missions.add(m);
        missions.add(m2);
        missions.add(m3);

        return missions;
    }

    private ArrayList<Item> createItems() {
        Item i = new Item();
        Item i2 = new Item();
        Item i3 = new Item();
        Item i4 = new Item();

        i.setCost(100);
        i2.setCost(0);
        i3.setCost(300);
        i4.setCost(350);

        ArrayList<Item> items = new ArrayList<>();
        items.add(i);
        items.add(i4);
        items.add(i2);
        items.add(i3);
        return items;
    }

    @Test
    public void testMissionSorting() {
        Assert.assertEquals(2, serviceHelper.sortMissions(createMissions()).size());
    }

    @Test
    public void testMissionSortingByQuiteLikely() {
        Assert.assertEquals(1, serviceHelper.sortMissionsByQuiteLikely(createMissions()).size());
    }

    @Test
    public void testGetMediumItemsList() {
        Assert.assertEquals(1, serviceHelper.getMediumItemsList(createItems()).size());
    }

    @Test
    public void testGetExpensiveItemsList() {
        Assert.assertEquals(2, serviceHelper.getExpensiveItemsList(createItems()).size());
    }
}
