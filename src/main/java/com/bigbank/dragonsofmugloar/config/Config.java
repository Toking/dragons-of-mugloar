package com.bigbank.dragonsofmugloar.config;

import com.bigbank.dragonsofmugloar.model.Game;
import com.bigbank.dragonsofmugloar.utils.RequestBuilder;
import com.bigbank.dragonsofmugloar.utils.ServiceHelper;
import com.bigbank.dragonsofmugloar.utils.URLBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan(basePackages = {"com.bigbank.dragonsofmugloar"})
public class Config {

    @Bean(name="serviceHelper")
    public ServiceHelper serviceHelper() {
        return new ServiceHelper();
    }

    @Bean(name="restTemplate")
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean(name="urlBuilder")
    public URLBuilder urlBuilder() {
        return new URLBuilder();
    }

    @Bean(name="requestService")
    public RequestBuilder requestService() {
        return new RequestBuilder();
    }

    @Bean(name="game")
    public Game game() {
        return new Game();
    }
}
