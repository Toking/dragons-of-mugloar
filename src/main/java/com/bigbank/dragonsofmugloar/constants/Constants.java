package com.bigbank.dragonsofmugloar.constants;

public class Constants {
    public static final int MIN_MONEY_TO_BUY_ITEM = 50;
    public static final int MIN_MONEY_TO_BUY_MEDIUM_ITEM = 100;
    public static final int MIN_MONEY_TO_BUY_EXPENSIVE_ITEM = 300;
    public static final String PIECE_OF_CAKE = "Piece of cake";
    public static final String WALK_IN_THE_PARK = "Walk in the park";
    public static final String SURE_THING = "Sure thing";
    public static final String QUITE_LIKELY = "Quite likely";
}
