package com.bigbank.dragonsofmugloar.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SolvedMission {
    private boolean success;
    private int lives;
    private int gold;
    private int score;
    private int highScore;
    private int turn;
    private String message;
}
