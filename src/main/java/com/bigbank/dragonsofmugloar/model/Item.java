package com.bigbank.dragonsofmugloar.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Item {
    private String id;
    private String name;
    private int cost;
}
