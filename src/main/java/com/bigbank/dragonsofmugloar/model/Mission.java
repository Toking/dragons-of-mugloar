package com.bigbank.dragonsofmugloar.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Mission {
    private String adId;
    private String message;
    private String reward;
    private int expiresIn;
    private String probability;
}
