package com.bigbank.dragonsofmugloar.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuyItemResponse {
    private String shoppingSuccess;
    private int gold;
    private int lives;
    private int level;
    private int turn;
}
