package com.bigbank.dragonsofmugloar.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Bean;

@Getter @Setter
public class Game {
    private String gameId;
    private int lives;
    private int gold;
    private int level;
    private int score;
    private int highScore;
    private int turn;
}
