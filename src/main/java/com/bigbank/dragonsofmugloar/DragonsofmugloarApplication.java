package com.bigbank.dragonsofmugloar;

import com.bigbank.dragonsofmugloar.service.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class DragonsofmugloarApplication {
	@Autowired
	CommandService service;

	public static void main(String[] args) {
		SpringApplication.run(DragonsofmugloarApplication.class, args);
	}

	@PostConstruct
	public void init() {
		service.startGame();
	}

}
