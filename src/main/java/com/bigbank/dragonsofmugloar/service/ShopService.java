package com.bigbank.dragonsofmugloar.service;

import com.bigbank.dragonsofmugloar.model.BuyItemResponse;
import com.bigbank.dragonsofmugloar.model.Item;
import com.bigbank.dragonsofmugloar.utils.RequestBuilder;
import com.bigbank.dragonsofmugloar.utils.ServiceHelper;
import com.bigbank.dragonsofmugloar.utils.URLBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import static com.bigbank.dragonsofmugloar.constants.Constants.*;

@Service
public class ShopService {
    private Logger logger = LoggerFactory.getLogger(ShopService.class);
    private ArrayList<Item> shopItems;

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("requestService")
    private RequestBuilder requestBuilder;

    @Autowired
    @Qualifier("urlBuilder")
    private URLBuilder urlBuilder;

    @Autowired
    @Qualifier("serviceHelper")
    private ServiceHelper serviceHelper;

    private ArrayList<Item> getShopItems(String gameId) {
        ResponseEntity<ArrayList<Item>> response = restTemplate.exchange(urlBuilder.getShopItemsURL(gameId),
                HttpMethod.GET, null, new ParameterizedTypeReference<ArrayList<Item>>(){});
        return response.getBody();
    }

    int makePurchase(String gameId, int gold, int lives) {
        if(shopItems == null || shopItems.isEmpty()) {
            shopItems = getShopItems(gameId);
        }
        if (lives < 2 && gold >= MIN_MONEY_TO_BUY_ITEM) {
            logger.info("Let's buy 1 life!");
            return buyLife(gameId, shopItems);
        } else if(gold >= MIN_MONEY_TO_BUY_EXPENSIVE_ITEM) {
            logger.info("Let's buy expensive item!");
            return buyExpensiveItem(gameId, shopItems, gold);
        } else if(gold >= MIN_MONEY_TO_BUY_MEDIUM_ITEM) {
            logger.info("Let's buy medium item!");
            return buyMediumItem(gameId, shopItems, gold);
        }
        return gold;
    }

    private int buyLife(String gameId, ArrayList<Item> items) {
        return buyItem(items.get(0), gameId);
    }

    private int buyMediumItem(String gameId, ArrayList<Item> items, int gold) {
        ArrayList<Item> mediumItems = serviceHelper.getMediumItemsList(items);
        if(mediumItems.isEmpty()) {
            logger.info("All the medium items are already bought!!!");
            return gold;
        } else {
            Item item = mediumItems.get(0);
            shopItems.remove(item);
            return buyItem(item, gameId);
        }
    }

    private int buyExpensiveItem(String gameId, ArrayList<Item> items, int gold) {
        ArrayList<Item> expensiveItems = serviceHelper.getExpensiveItemsList(items);
        if(expensiveItems.isEmpty()) {
            logger.info("All the expensive items are already bought!!!");
            return gold;
        } else {
            Item item = expensiveItems.get(0);
            shopItems.remove(item);
            return buyItem(item, gameId);
        }
    }

    private int buyItem(Item item, String gameId) {
        BuyItemResponse response = restTemplate.postForObject(urlBuilder.getBuyItemURL(gameId, item.getId()),
                requestBuilder.createRequest(), BuyItemResponse.class);

        if(Boolean.parseBoolean(response.getShoppingSuccess())) {
            logger.info("Item " + item.getName() + " was bought successfully for " + item.getCost());
        } else {
            logger.info("Item was not bought!!!");
        }
        return response.getGold();
    }

}
