package com.bigbank.dragonsofmugloar.service;

import com.bigbank.dragonsofmugloar.model.Game;
import com.bigbank.dragonsofmugloar.model.Mission;
import com.bigbank.dragonsofmugloar.model.SolvedMission;
import com.bigbank.dragonsofmugloar.utils.RequestBuilder;
import com.bigbank.dragonsofmugloar.utils.ServiceHelper;
import com.bigbank.dragonsofmugloar.utils.URLBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import static com.bigbank.dragonsofmugloar.constants.Constants.*;

@Service
public class CommandService {

    private Logger logger = LoggerFactory.getLogger(CommandService.class);

    private ArrayList<Mission> missions;

    private int money = 0;

    private int round = 1;

    @Autowired
    ShopService shopService;

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    @Qualifier("serviceHelper")
    private ServiceHelper serviceHelper;

    @Autowired
    @Qualifier("urlBuilder")
    private URLBuilder urlBuilder;

    @Autowired
    @Qualifier("game")
    private Game game;

    @Autowired
    @Qualifier("requestService")
    private RequestBuilder requestBuilder;

    public void startGame() {
        game = restTemplate.postForObject(urlBuilder.getStartGameURL(), requestBuilder.createRequest(), Game.class);
        logger.info("\n Game " + game.getGameId() + " has started with " + game.getGold() + " gold \n");
        runGameCycle();
    }

    private void runGameCycle() {
        logger.info("*** Cycle # " + round);
        missions = (serviceHelper.sortMissions(getMissions()).isEmpty() ? serviceHelper.sortMissionsByQuiteLikely(getMissions())
                : serviceHelper.sortMissions(getMissions()));
        if (missions.isEmpty()) {
            missions = getMissions();
        }
        missions.forEach(this::solveMission);
        round++;
        logger.info("\n");
        runGameCycle();

    }

    private void solveMission(Mission mission) {

        try {
            SolvedMission result = restTemplate.postForEntity(urlBuilder.getSolveTaskURL(game.getGameId(),
                    mission.getAdId()), requestBuilder.createRequest(), SolvedMission.class).getBody();

            this.money = result.getGold();
            logger.info("*** Go to mission with probability: " + mission.getProbability());
            if(isFailed(result)) {
                logger.info("* - Failed, lives: " + result.getLives() + ", gold: " + money + ", Total Score: "  + result.getScore());
                if (isDead(result.getLives())) {
                    logger.info("\n");
                    logger.info("* - Game over. Total Score: " + result.getScore() + ", Gold: " + money);
                    System.exit(0);
                }
                if (money >= MIN_MONEY_TO_BUY_ITEM) {
                    money = shopService.makePurchase(game.getGameId(), money, result.getLives());
                } else {
                    logger.info("* - You do not have money for making purchase, you balance is: " + money);
                }
            } else  {
                logger.info("* + Success: + " + mission.getReward() + ", Reward: " + mission.getReward() + ", Total Score: "  + result.getScore() +  " Gold: " + money + ", Lives: " + result.getLives());
                money = shopService.makePurchase(game.getGameId(), money, result.getLives());
            }

        } catch (HttpClientErrorException e) {
//            logger.warn("::: " + e.getStatusText() + ", code: " + e.getRawStatusCode());
        }
    }

    private ArrayList<Mission> getMissions() {
        return restTemplate.exchange(urlBuilder.getMissionsURL(game.getGameId()), HttpMethod.GET,
                null, new ParameterizedTypeReference<ArrayList<Mission>>(){}).getBody();
    }

    private boolean isFailed(SolvedMission result) {
        return !result.isSuccess();
    }

    private boolean isDead(int lives) {
        return lives == 0;
    }
}
