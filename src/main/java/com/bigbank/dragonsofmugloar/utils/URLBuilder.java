package com.bigbank.dragonsofmugloar.utils;

public class URLBuilder {
    private static final String URL = "https://dragonsofmugloar.com/api/v2/";
    public String getStartGameURL() { return URL + "game/start";}
    public String getMissionsURL(String gameId) { return URL + gameId + "/messages";}
    public String getShopItemsURL(String gameId) { return URL + gameId + "/shop";}
    public String getBuyItemURL(String gameId, String itemId) { return URL + gameId + "/shop/buy/" + itemId; }
    public String getSolveTaskURL(String gameId, String taskId) { return URL + gameId + "/solve/" + taskId;}
}
