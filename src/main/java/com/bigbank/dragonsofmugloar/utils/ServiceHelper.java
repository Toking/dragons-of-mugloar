package com.bigbank.dragonsofmugloar.utils;

import com.bigbank.dragonsofmugloar.model.Item;
import com.bigbank.dragonsofmugloar.model.Mission;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static com.bigbank.dragonsofmugloar.constants.Constants.*;
import static com.bigbank.dragonsofmugloar.constants.Constants.QUITE_LIKELY;

public class ServiceHelper {

    public ArrayList<Mission> sortMissions(ArrayList<Mission> missions) {
        return  (ArrayList<Mission>) missions.stream()
                .filter(s -> s.getProbability().equals(PIECE_OF_CAKE) ||
                        s.getProbability().equals(WALK_IN_THE_PARK) ||
                        s.getProbability().equals(SURE_THING)).collect(Collectors.toList());
    }

    public ArrayList<Mission> sortMissionsByQuiteLikely(ArrayList<Mission> missions) {
        return  (ArrayList<Mission>) missions.stream()
                .filter(s -> s.getProbability().equals(QUITE_LIKELY)).collect(Collectors.toList());
    }

    public ArrayList<Item> getMediumItemsList(ArrayList<Item> items) {
        return (ArrayList<Item>) items.stream()
                .filter(s -> s.getCost() >= MIN_MONEY_TO_BUY_MEDIUM_ITEM && s.getCost() < MIN_MONEY_TO_BUY_EXPENSIVE_ITEM)
                .collect(Collectors.toList());
    }

    public ArrayList<Item> getExpensiveItemsList(ArrayList<Item> items) {
        return (ArrayList<Item>)items.stream()
                .filter(s -> s.getCost() >= MIN_MONEY_TO_BUY_EXPENSIVE_ITEM)
                .collect(Collectors.toList());
    }
}
