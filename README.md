#SETUP

- Install JAVA 8 and MAVEN

- Import project with a help of maven

- Lombok and Annotations proccess should be enabled in Setting of Intellij

#START GAME
- Run application from class DragonsofmugloarApplication

- Application proccess will be shown as log notes in console

- Wait till the line 'Game over...' where will be shown the result

#THE HIGHEST RECORD WAS: 6623 POINTS 

#EXAMPLE OF GAME ENDING:

2019-09-09 11:14:51.344  INFO 15852 --- [           main] c.b.d.s.CommandService                   : *** Cycle # 32
2019-09-09 11:14:51.500  INFO 15852 --- [           main] c.b.d.s.CommandService                   : *** Go to mission with probability: Impossible
2019-09-09 11:14:51.500  INFO 15852 --- [           main] c.b.d.s.CommandService                   : * - Failed, lives: 1, gold: 73, Total Score: 4773
2019-09-09 11:14:51.500  INFO 15852 --- [           main] c.b.d.s.ShopService                      : Let's buy 1 life!
2019-09-09 11:14:51.540  INFO 15852 --- [           main] c.b.d.s.ShopService                      : Item Healing potion was bought successfully for 50
2019-09-09 11:14:51.623  INFO 15852 --- [           main] c.b.d.s.CommandService                   : *** Go to mission with probability: Impossible
2019-09-09 11:14:51.623  INFO 15852 --- [           main] c.b.d.s.CommandService                   : * - Failed, lives: 1, gold: 23, Total Score: 4773
2019-09-09 11:14:51.623  INFO 15852 --- [           main] c.b.d.s.CommandService                   : * - You do not have money for making purchase, you balance is: 23
2019-09-09 11:14:51.702  INFO 15852 --- [           main] c.b.d.s.CommandService                   : *** Go to mission with probability: Impossible
2019-09-09 11:14:51.702  INFO 15852 --- [           main] c.b.d.s.CommandService                   : * - Failed, lives: 0, gold: 23, Total Score: 4773
2019-09-09 11:14:51.702  INFO 15852 --- [           main] c.b.d.s.CommandService                   : 

2019-09-09 11:14:51.702  INFO 15852 --- [           main] c.b.d.s.CommandService                   : * - Game over. Total Score: 4773, Gold: 23